/** @format */
// Higher Order Component
import React, { useState } from 'react';

export const TodoListContext = React.createContext();

const TodoListProvider = ({ children }) => {

  const [todolist, setTodolist] = useState([]);

  function handelItemAdd(todo){
    setTodolist([...todolist,todo])
  }

  function handleItemRemove(items){
    setTodolist(items)
  }

  return (
    <TodoListContext.Provider value={{ todolist, handelItemAdd, handleItemRemove }}>
      {children}
    </TodoListContext.Provider>
  );
};

export default TodoListProvider;