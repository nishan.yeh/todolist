import React from 'react'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect'
import TodoButton from './button';

describe('測試Button', () => {

  test('當按下按鈕時，mockFunc會被呼叫', () => {
    // Arrange
    const mockFunc = jest.fn();

    render(
      <TodoButton 
        onButtonClick={mockFunc} // 小心onButtonClick不要打錯
      />   
    ); 
    
    // 延伸問題：按兩次，還是會隨機跑N次？

    // Arrange
    const todoButton = screen.getByText('Add todo')// 用文字去抓
    // Act
    userEvent.click(todoButton);
    //Assert
    expect(mockFunc.mock.calls.length).toBe(1);

    // Act
    userEvent.click(todoButton);
    //Assert
    expect(mockFunc.mock.calls.length).toBe(2);

  });
});
