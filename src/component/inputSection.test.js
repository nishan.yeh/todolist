import React from 'react'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect'

import InputSection from './inputSection';

describe('測試inputSection', () => {
  
  test('輸入一段文字的時候，input的value真的有那段文字', () => {
    const mockFunc = jest.fn();

    render(
      <InputSection
        onItemAdd={mockFunc}
      />
    );
  
    // Arrange
    const inputNode = screen.getByLabelText('todo-input') 
  
    // Act
    userEvent.type(inputNode, 'Hello, World!')

    // Assert
    expect(inputNode.value).toBe= 'Hello, World!'
  })


  test('當輸入一段文字，並且按下按鈕時，mockFunc會被呼叫', () => {
    const mockFunc = jest.fn();

    render(
      <InputSection
        onItemAdd={mockFunc}
      />
    );

    // Arrange
    const inputNode = screen.getByLabelText('todo-input') //用label去拿到input的值
    const buttonNode = screen.getByText('Add todo') // 用文字去抓
  
    // Act
    userEvent.type(inputNode, 'Hello, World!')
    userEvent.click(buttonNode)

    // Assert
    expect(mockFunc.mock.calls.length).toBe(1);
    expect(mockFunc.mock.calls[0][0]).toBe('Hello, World!');
  })
  
});
