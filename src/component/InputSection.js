import React,{ useState }  from 'react';
import TodoButton from './button' 
import { connect } from 'react-redux'
import { addTodo } from '../actions'


const InputSection = ({ dispatch }) => {

  const [todo, setTodo] = useState('');

  // 可以同時使用 Redux 和 hook 嗎？

  function handleInputChangeValue(event){
    setTodo(event.target.value)
  }

  return (
    <div>
      <input type="text" 
        aria-label="todo-input"
        value={todo} onChange={handleInputChangeValue} />
      <TodoButton 
        onButtonClick={dispatch(addTodo(todo))}
      />
    </div>
  )
}

// 在這邊引入Action，然後加上todo，不知寫法是否正確？

export default connect()(InputSection)