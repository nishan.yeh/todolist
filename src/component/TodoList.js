import React from 'react';
import { connect } from 'react-redux'
import { removeTodo } from '../actions'

const TodoList = ({ dispatch }) => {

  const removeStyle = {
    marginLeft: '60px',
    border:'1px solid black'
  }

  // 在這邊引入Action，可以Remove todo ?
  function renderTodoList(value,index){
    return (
    <li key={index}>
      {value}  
      <span 
        aria-label={"remove-"+index}
        onClick={()=>dispatch(removeTodo(index))}
        style={removeStyle}>x
      </span>
    </li>
    )
  }

  // 如何在這邊獲取 Store 的資料？
  return (
    <ul>
      {TodoListObject.todolist.map(renderTodoList)} 
    </ul>
  )
}

  // 如何拿到 Store 的資料？ 不確定如何傳入參數？
  const TodoList = connect(
    mapStateToProps,
    mapDispatchToProps
  )(TodoList)

export default TodoList;