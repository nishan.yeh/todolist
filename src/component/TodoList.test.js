import React from 'react'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect'

import TodoList from './TodoList';

describe('測試TodoList', () => {

  test('todo的被點X的時候，handleItemRemove會被正確呼叫', () => {

    const mockFunc = jest.fn();

    render(
      <TodoList
        todolist = {['a','b','c']}
        handleItemRemove={mockFunc}
      />
    );

    const removeItem = screen.getByLabelText('remove-2')
    userEvent.click(removeItem)
    expect(mockFunc.mock.calls.length).toBe(1);

  })

  test('array有幾個todo，就要render幾個item', () => {

    const mockFunc = jest.fn();

    render(
      <TodoList
        todolist = {['a','b','c']}
        handleItemRemove={mockFunc}
      />
    );

    const items = screen.getAllByRole('listitem')
    expect(items.length).toBe(3); 

  })

  
});
