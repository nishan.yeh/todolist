
export const addTodo = todo => ({
  type: 'ADD_TODO',
  todo
})

export const removeTodo = index => ({
  type: 'REMOVE_TODO',
  index
})


// 不知道Action，這樣的設計是否正確？