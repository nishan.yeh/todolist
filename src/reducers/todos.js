
function todos(state = [], action) {
  switch (action.type) {
    case 'ADD_TODO':
      return [...state, action.todo]
    case 'REMOVE_TODO':
      return state.filter((_ , itemIndex) => itemIndex !== action.index)
    default:
      return state
  }
}

export default todos

// 不知道這樣的設計是否正確？
// state 的儲存就是在reducer嗎？