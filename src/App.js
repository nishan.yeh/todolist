import React  from 'react';
import './App.css';
import InputSection from './component/InputSection'
import TodoList from './component/TodoList'
import TodoListProvider from './provider/TodoListProvider';

function App() {

  return (
    <TodoListProvider>  
      <div>
        <h1>Todo list</h1>
        <InputSection/>
        <TodoList/>
      </div>
    </TodoListProvider> 
  )
}

export default App;