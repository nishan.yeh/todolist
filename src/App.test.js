import React from 'react'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect'

import App from './App';

  // 1.輸入文字
  // 2.按按下按鈕
  // 3.li有出現正確的數字 ＆ 輸入的文字跟li裡面的內容是一樣的
  
  // 未完成：要測試可以刪除


describe('測試APP', () => {

  test('沒有任何todo', () => {

    render(
      <App/>
    )
    const items = screen.queryAllByRole('listitem') 
    expect(items.length).toBe(0);

  })

  test('成功創造2個todo', () => {

    render(
      <App/>
    )

    const inputNode = screen.getByLabelText('todo-input')
    const buttonNode = screen.getByText('Add todo')

     //驗證出現正確的文字
    userEvent.type(inputNode, 'test')
    userEvent.click(buttonNode)
    const case1 = screen.getAllByText('test')   
    expect(case1.length).toBe(1)

    userEvent.type(inputNode, 'todo')
    userEvent.click(buttonNode)
    const case2 = screen.getAllByText('todo')   
    expect(case2.length).toBe(1)     

    //驗證出現多少<li>個 （正常會這樣做嗎?）
    const items = screen.getAllByRole('listitem') 
    expect(items.length).toBe(2);

  })


});
