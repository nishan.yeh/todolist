
import React , { useState }  from 'react';

const [todolist, setTodolist] = useState([]);

function handelItemAdd(todo){
  console.log('handelItemAdd',todo)
  setTodolist([...todolist,todo])
}

function handleItemRemove(items){
  setTodolist(items)
}

const todo = {
  handelItemAdd:handelItemAdd,
  handleItemRemove:handleItemRemove
};

const todoContext = React.createContext(todo); 


export default todoContext

